import {Row, Col, Card, Container} from 'react-bootstrap'

export default function About() {
	return (
		<Container className = "aboutMe container-fluid min-vh-100">
			<Row className= "pt-5">
			<Col>
			
				<h1 className="my-3 about"> ABOUT ME </h1>
				<h2 className="my-4 mx-4">John Louie Sequena</h2>
				<h4 className="my-4 mx-4">Full Stack Web Developer</h4>
				<p className="my-4 mx-4">	A career shifter fascinated and in awe of exploring the good scenery in our nature as well as the new technology innovations that are evolving so fast. I am eager to study and take part in the next cool tech experience. Constantly learning motivates me to explore more and appreciate the spaces where this technology could take us.</p>
				<h5>Contact Me</h5>
				<ul>Email: sequena_jl@yahoo.com</ul>
				<ul>Mobile No. 09192780343</ul>
				<ul>Address: Calamba Laguna Philippines</ul>
			</Col>
			</Row>
		</Container>
		)
}