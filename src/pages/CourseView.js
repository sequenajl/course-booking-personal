//this will serve as page whenever the client would want to select a single course/item from the catalog
import { useState, useEffect } from 'react';
import Hero from '../components/Banner';
//Grid System, Card, Button
import {Row, Col, Card, Button, Container} from 'react-bootstrap';

//declare a state for the course details. we need to use the correct 'hook'

//import sweetalert
import Swal from 'sweetalert2';

//routing component
import {Link, useParams } from 'react-router-dom';

const data = {
	title: 'Welcome to B156 Booking-App',
	content: 'Checkout our school campus'
}

export default function CourseView() {

	//state of our course details
	const [courseInfo, setCourseInfo] = useState({
		name: null,
		description: null,
		price: null
	});

	//retrieve the data from the url to extract the course id that will be used as a reference to determine which course will be displayed by the page. acquire a 'Hook' which will allow us to manage/access the data in the browser URL.

	//using the parameter Hook(useParams) -> this will be provided by 'react-router-dom'
	//(useState, useEffect, useParams)

	//the useParams will return an object that will contain the path variables stored in the URL.
	//take a peek at the data inside the URL
	console.log(useParams());

	//extract the value from the path variables by destructuring the object
	const {id} = useParams()
	console.log(id);
	//create a "side effect" which will send a request to our backend API for course-booking. use the proper 'hook' (effect hook).

	useEffect(()=>{

		//send a request to our API to retrieve information about the course.
		//syntax: fetch('<URL address>','<OPTIONS>')
		//upon sending this req to the api a promise will be initialized.
		fetch(`https://morning-earth-29301.herokuapp.com/courses/${id}`).then(res => res.json()).then(convertedData =>{
			// console.log(convertedData);
			//the data that we retrieved from the db, we need to contain 
			//call the setter to change the state of the course info for this page
			setCourseInfo({
				name: convertedData.name,
				description: convertedData.description,
				price: convertedData.price
			})
		});
	},[id])




	const enroll = () => {
		return (
			Swal.fire({
				icon: "success",
				title: 'Enrolled Successfully!',
				text: 'Thank you for enrolling to this course'
			})

		);
	};

	return(
		<>
			<Hero bannerData={data} />
			<Row>
				<Col>
					<Container>
						<Card className="text-center">
							<Card.Body>
								{/*Course Name*/}
								<Card.Title>
									<h2> {courseInfo.name}</h2>
								</Card.Title>
								{/*Course Description*/}
								<Card.Subtitle>
									<h6 className="my-4"> Description: </h6>
								</Card.Subtitle>
								<Card.Text>
									{courseInfo.description}
								</Card.Text>
								{/*Course Price*/}
								<Card.Subtitle>
									<h6 className="my-4"> Price: ₱{courseInfo.price} </h6>
								</Card.Subtitle>
							</Card.Body>
							<Button variant="warning" className="btn-block" onClick={enroll} >
							 Enroll
							</Button>
							<Link className="btn btn-info btn-block mb-5" to="/login">
								Login to Enroll
							</Link>
						</Card>		
					</Container>		
				</Col>			
			</Row>
		</>
		);
};