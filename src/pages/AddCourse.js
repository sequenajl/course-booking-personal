//components needed to create AddCourse
import Hero from '../components/Banner';
import {Container, Form, InputGroup, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';

const data = {
	title: 'Create Course Page',
	content: 'Input course details here'
}

export default function addCourse() {

	const createCourse = (event) => {
		event.preventDefault()
		return(
			//display a message that will confirm to the user that registration is successful.
			Swal.fire({
				icon: 'success',
				title: 'Successfully Created Course',
			})
		);
};
	return (
		<div>
			<Hero bannerData={data} />

			<Container>
				<h1 className="text-center">Add Course</h1>
				<Form onSubmit={e=>createCourse(e)} >
					{/*Course Name Field*/}
					<Form.Group>
						<Form.Label>Course Name:</Form.Label>
						<Form.Control type="text" placeholder="Enter Course Name" required/>
					</Form.Group>
					{/*Course Description Field*/}
					<Form.Group>
						<Form.Label>Course Description:</Form.Label>
						<Form.Control type="text" placeholder="Enter Course Description" required/>
					</Form.Group>
				
					{/*Price Field*/}
						<Form.Label>Price:</Form.Label>
					<InputGroup>
						<InputGroup.Text>PHP</InputGroup.Text>
						<Form.Control type="number" placeholder="Enter Price" required/>
					</InputGroup>
					{/*Add Course Button*/}
					<Button type="submit" className="btn-info mb-3 my-3">Create Course</Button>
				</Form>
			</Container>



		</div>



		);
	};

